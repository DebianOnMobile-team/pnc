add_library(gawkpn
	SHARED
		gawkpnwrap.c gawkpnimpl.cc
)

set_target_properties(gawkpn PROPERTIES PREFIX "")

target_link_libraries(gawkpn
	${LIBICUUC} ${LIBPHONENUMBER} ${LIBGEOCODING}
)

execute_process(
	COMMAND
		gawk "BEGIN {print ENVIRON[\"AWKLIBPATH\"]}"
	OUTPUT_VARIABLE
		AWKLIBPATH
	OUTPUT_STRIP_TRAILING_WHITESPACE
)
install(
	TARGETS gawkpn
	DESTINATION ${AWKLIBPATH}
)
