/*
 * gawkpn - a gawk extension for phone number manipulation
 *
 * Copyright 2023 Sxmo Contributors
 * <sxmo.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SRC_GAWKPNIMPL_H_
#define SRC_GAWKPNIMPL_H_

#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

struct str_list {
	struct str_list *prev;
	size_t len;
	char str[1];
};

enum number_type {
  FIXED_LINE,
  MOBILE,
  FIXED_LINE_OR_MOBILE,
  TOLL_FREE,
  PREMIUM_RATE,
  SHARED_COST,
  VOIP,
  PERSONAL_NUMBER,
  PAGER,
  UAN,
  VOICEMAIL,
  UNKNOWN
};

struct pn_info {
	uint32_t country_code;
	char country_name[2];
	enum number_type type;
	char *location;
	size_t location_len;
	uint32_t possible_short_number:1;
	uint32_t valid_short_number:1;
	uint32_t emergency_number:1;
};

int pn_init(void);

int pn_format(char *num, size_t num_len, char **res, size_t *res_len);

int pn_valid(char *num, size_t num_len);

struct str_list *pn_find(char *text, size_t text_len);

int pn_info(char *num, size_t num_len, struct pn_info* res);

int set_format(char *str, size_t len);

int set_leniency(char *str, size_t len);

void set_country(char *str, size_t len);

void pn_dialout(char *num, size_t num_len, char *country_code, size_t cc_len, char **res, size_t *res_len);

#ifdef __cplusplus
}
#endif

#endif /* SRC_GAWKPNIMPL_H_ */
