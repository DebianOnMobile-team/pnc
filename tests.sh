#!/bin/sh
pnc="${1:-$PWD/build}/pnc"
set -ex

"$pnc" valid +33123456789
"$pnc" valid -c US '(202) 555-0110'

"$pnc" valid '(202) 555-0110' && exit 1

[ "$("$pnc" format "+1 20 2 555    01 10")"    = "+12025550110" ]
[ "$("$pnc" format -c FR "0123456789")"        = "+33123456789" ]
[ "$("$pnc" format -c FR -f nat "0123456789")" = "01 23 45 67 89" ]
[ "$("$pnc" format -f int "+12025550110")"     = "+1 202-555-0110" ]

[ "$("$pnc" find -c "" "+330611234345")" = "+33611234345" ]

[ "$("$pnc" find -f nat "2017/04/20: You have 2 messsages, call +1-202-555-0110 to listen to them.")" = "(202) 555-0110" ]

[ "$("$pnc" find -c FR "rappelle-moi au 01 23 4 56789 ou au 06 78 90 12 34 après 20h00" | head -1)" = "+33123456789" ]
[ "$("$pnc" find -c FR "rappelle-moi au 01 23 4 56789 ou au 06 78 90 12 34 après 20h00" | tail -1)" = "+33678901234" ]
